// import the required packages from the AWS SDK for Java
import com.amazonaws.services.cognitoidentity.*;
import com.amazonaws.services.cognitoidentity.model.*;
import com.amazonaws.services.securitytoken.*;
import com.amazonaws.services.securitytoken.model.*;
import com.amazonaws.services.cognitosync.*;
import com.amazonaws.services.cognitosync.model.*;
import com.amazonaws.auth.*;
import java.util.*;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;

public class CognitoSample {
    
	public static final String AWS_ACCOUNT_ID = "YOURID";
	public static final String COGNITO_POOL_ID = "us-east-1:YOUR_POOL_NAME";
	public static final String COGNITO_ROLE_AUTH = "arn:aws:iam::COGNITO_AUTH_ROLE";
	public static final String COGNITO_ROLE_UNAUTH = "arn:aws:iam::COGNITO_UNAUTH_ROLE";
    public static final String AWS_KEY = "AWSKEY";
    public static final String AWS_SECRET = "AWSSECRET";
    public static final String COGNITO_DEV_IDENTITY = "test_idpool_1_dev";
	public static final String COGNITO_SYNC_DATASET_NAME = "test_dataset1";
    
    public static String COGNITO_AUTHENTICATED_USER_PREFIX = "userid-";

    public static void main(String args[]) {
        log("Adding Anonymous User...");
        new CognitoSample().addAnonymoususer();
        
        log("Adding Authenticated User...");
        new CognitoSample().addAuthenticatedUser();
        // new CognitoSample().addAuth2();
    }

    public static void log(Object msg) {
        System.out.println("" + msg);
    }
    
    public void addAnonymoususer() {
        try {
            AmazonCognitoIdentity identityClient = new AmazonCognitoIdentityClient(new AnonymousAWSCredentials());
            
            GetIdRequest idRequest = new GetIdRequest();
            idRequest.setAccountId(AWS_ACCOUNT_ID);
            idRequest.setIdentityPoolId(COGNITO_POOL_ID);
            // idRequest.setLogins(providerTokens);
            GetIdResult idResp = identityClient.getId(idRequest);
            String identityId = idResp.getIdentityId();
            log("Unauthenticated id: " + identityId);
            
            
            // Create the request object
            GetOpenIdTokenRequest tokenRequest = new GetOpenIdTokenRequest();
            tokenRequest.setIdentityId(identityId);
            // If you are authenticating your users through an identity provider
            // then you can set the Map of tokens in the request
            Map providerTokens = new HashMap();
            // providerTokens.put("com.mycustom.anonymous.user", "somecustomvaluehere");
            tokenRequest.setLogins(providerTokens);
            GetOpenIdTokenResult tokenResp = identityClient.getOpenIdToken(tokenRequest);
            // get the OpenID token from the response
            String openIdToken = tokenResp.getToken();
            
            
            // you can now create a set of temporary, limited-privilege credentials to access
            // your AWS resources through the Security Token Service utilizing the OpenID
            // token returned by the previous API call. The IAM Role ARN passed to this call
            // will be applied to the temporary credentials returned
            AWSSecurityTokenService stsClient = new AWSSecurityTokenServiceClient(new AnonymousAWSCredentials());
            AssumeRoleWithWebIdentityRequest stsReq = new AssumeRoleWithWebIdentityRequest();
            stsReq.setRoleArn(COGNITO_ROLE_UNAUTH);
            stsReq.setWebIdentityToken(openIdToken); //awsAccessToken);
            stsReq.setRoleSessionName("AppTestSession");

            AssumeRoleWithWebIdentityResult stsResp = stsClient.assumeRoleWithWebIdentity(stsReq);
            com.amazonaws.services.securitytoken.model.Credentials stsCredentials = stsResp.getCredentials();

            // Create the session credentials object
            AWSSessionCredentials sessionCredentials = new BasicSessionCredentials(
                stsCredentials.getAccessKeyId(),
                stsCredentials.getSecretAccessKey(),
                stsCredentials.getSessionToken()
            );
            // save the timeout for these credentials 
            Date sessionCredentialsExpiration = stsCredentials.getExpiration();
            
            // these credentials can then be used to initialize other AWS clients,
            // for example the Amazon Cognito Sync client
            AmazonCognitoSync syncClient = new AmazonCognitoSyncClient(sessionCredentials);
            ListDatasetsRequest syncRequest = new ListDatasetsRequest();
            syncRequest.setIdentityId(idResp.getIdentityId());
            syncRequest.setIdentityPoolId(COGNITO_POOL_ID);
            ListDatasetsResult syncResp = syncClient.listDatasets(syncRequest);
            
            // syncClient.openOrCreateDataset(COGNITO_SYNC_DATASET_NAME);
        } catch(Exception ex) {
            log("Exception in addAnonymoususer()");
            ex.printStackTrace();
        }
    }

    // public void addAuth2() {
        // BasicAWSCredentials credentials = new BasicAWSCredentials(AWS_KEY, AWS_SECRET);
        // AmazonCognitoIdentityClient client = new AmazonCognitoIdentityClient(credentials);
        // GetOpenIdTokenForDeveloperIdentityRequest tokenRequest = new GetOpenIdTokenForDeveloperIdentityRequest();
        // tokenRequest.setIdentityPoolId(COGNITO_POOL_ID);
        // HashMap<String, String> map = new HashMap<String, String>();

        // map.put(COGNITO_DEV_IDENTITY, "nameid.number@provider.com");

        // //Duration of the generated OpenID Connect Token
        // tokenRequest.setLogins(map);

        // tokenRequest.setTokenDuration(1000l);

        // GetOpenIdTokenForDeveloperIdentityResult result = client.getOpenIdTokenForDeveloperIdentity(tokenRequest);
        // String identityId = result.getIdentityId();
        // String token = result.getToken();
    // }
        
    public void addAuthenticatedUser() {
        log("Inside addAuthenticatedUser()");
        try {
            AWSCredentials credentials = new BasicAWSCredentials(AWS_KEY, AWS_SECRET);
            AmazonCognitoIdentity client = new AmazonCognitoIdentityClient(credentials); //new AnonymousAWSCredentials());
            // AmazonCognitoIdentityClient client = new AmazonCognitoIdentityClient(credentials);
            GetOpenIdTokenForDeveloperIdentityRequest tokenRequest =  new GetOpenIdTokenForDeveloperIdentityRequest();
            tokenRequest.setIdentityPoolId(COGNITO_POOL_ID);
            HashMap<String, String> map = new HashMap<String, String>();

            //Key -> Developer Provider Name used when creating the identity pool
            //Value -> Unique identifier of the user in your <u>backend</u>
            map.put(COGNITO_DEV_IDENTITY, getUsername());

            //Duration of the generated OpenID Connect Token
            tokenRequest.setLogins(map); 
            tokenRequest.setTokenDuration(1000l);

            GetOpenIdTokenForDeveloperIdentityResult result = client.getOpenIdTokenForDeveloperIdentity(tokenRequest);
            String identityId = result.getIdentityId();
            log("IdentityId=" + identityId);
            String token = result.getToken();
            log("Dev Id Token=" + token);
            
            // initialize the Cognito identity client with a set
            // of anonymous AWS credentials
            AmazonCognitoSyncClient syncClient = new AmazonCognitoSyncClient(credentials);

            ListRecordsRequest recordsRequest = new ListRecordsRequest();
            recordsRequest.setDatasetName(COGNITO_SYNC_DATASET_NAME);
            recordsRequest.setIdentityId(identityId);
            recordsRequest.setIdentityPoolId(COGNITO_POOL_ID);
            ListRecordsResult records = syncClient.listRecords(recordsRequest);
            String syncSessionToken = records.getSyncSessionToken();
            // we can start creating the wrRecord object for the record we want to change/insert
            RecordPatch wrRecord = new RecordPatch();
            wrRecord.setOp(Operation.Replace);
            wrRecord.setKey("WYN_REWARDS_ID");
            String wrIdentity = generateRandomWRIdentity();
            wrRecord.setValue(wrIdentity); 
            wrRecord.setSyncCount((long) 0);
            // now we loop over the records to find if the record already existed and set the parameters we need
            List <Record>allRecords = records.getRecords();
            for (Record curRecord : allRecords) {
                // we already had the record, update to the current value
                if (curRecord.getKey().equals("WYN_REWARDS_ID")) {
                    wrRecord.setValue(wrIdentity);
                    wrRecord.setSyncCount(curRecord.getSyncCount());
                }
            }
            RecordPatch cognitoRecord = new RecordPatch();
            cognitoRecord.setOp(Operation.Replace);
            cognitoRecord.setKey("COGNITO_ID");
            cognitoRecord.setValue(identityId); 
            cognitoRecord.setSyncCount((long) 0);
            // now we loop over the records to find if the record already existed and set the parameters we need
            allRecords = records.getRecords();
            for (Record curRecord : allRecords) {
                // we already had the record, update to the current value
                if (curRecord.getKey().equals("COGNITO_ID")) {
                    cognitoRecord.setValue(identityId);
                    cognitoRecord.setSyncCount(curRecord.getSyncCount());
                }
            }
            ArrayList patches = new ArrayList();
            patches.add(wrRecord);
            patches.add(cognitoRecord);
            // now we can send the update records request
            UpdateRecordsRequest updateRequest = new UpdateRecordsRequest();
            updateRequest.setDatasetName(COGNITO_SYNC_DATASET_NAME);
            updateRequest.setIdentityId(identityId);
            updateRequest.setIdentityPoolId(COGNITO_POOL_ID);
            updateRequest.setSyncSessionToken(syncSessionToken);
            updateRequest.setRecordPatches(patches);
            UpdateRecordsResult output = syncClient.updateRecords(updateRequest);
            log(output.toString());
            
            
            // log("Trying to add CognitoID...");
            // // // send a get id request. This only needs to be executed the first time
            // // // and the result should be cached.
            // GetIdRequest idRequest = new GetIdRequest();
            // idRequest.setAccountId(AWS_ACCOUNT_ID);
            // idRequest.setIdentityPoolId(COGNITO_POOL_ID);
            // // //us-east-1:6944dd67-b58d-4de4-9918-0d7195d47720"); //YOUR_COGNITO_IDENTITY_POOL_ID");

            // // // If you are authenticating your users through an identity provider
            // // // then you can set the Map of tokens in the request
            // Map providerTokens = new HashMap();
            // providerTokens.put("somekey1", "somevalue1");
            // idRequest.setLogins(providerTokens);

            // GetIdResult idResp = client.getId(idRequest);

            // String identityId2 = idResp.getIdentityId();
            // log("identityId2=" + identityId2);

        } catch(Exception ex) {
            log("Exception in addAuthenticatedUser()");
            ex.printStackTrace();
        }
        log("Exiting addAuthenticatedUser()");
    }
    
    public static String getUsername() {
        String uuid = UUID.randomUUID().toString();
        String username = COGNITO_AUTHENTICATED_USER_PREFIX + uuid.substring(0, uuid.indexOf("-"));
        log("Username=" + username);
        
        return username;
    }

    public static String generateRandomWRIdentity() {
        int len = 8;
        SecureRandom rs = new SecureRandom();
        int maxIterations=1000;
        String s="";
        int i=0;
        while(s.length()<len) {
            s+=(String) new BigInteger(230, rs).toString(len);
            i++;
        }
        log("Generating Wyndham Rewards Id: " + s.substring(0, len));
        return s.substring(0, len);
    }
}
