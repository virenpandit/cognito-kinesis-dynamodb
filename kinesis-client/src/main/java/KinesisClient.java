import java.net.InetAddress;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.kinesis.AmazonKinesis;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.services.kinesis.model.ResourceNotFoundException;

public class KinesisClient {

    public static final String SAMPLE_APPLICATION_STREAM_NAME = "test-stream1";

    private static final String SAMPLE_APPLICATION_NAME = "SampleKinesisApplication";
    
    public static void log(Object msg) {
        System.out.println("" + msg);
    }
    
    public static void main(String args[]) {
        new KinesisClient().process();
    }

    private static AWSCredentialsProvider credentialsProvider;
    
    private static final InitialPositionInStream SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM = InitialPositionInStream.TRIM_HORIZON;
    
    public void process() {
        try {
            init();

            String workerId = InetAddress.getLocalHost().getCanonicalHostName() + ":" + UUID.randomUUID();
            KinesisClientLibConfiguration kclConfig = new KinesisClientLibConfiguration(SAMPLE_APPLICATION_NAME,
                                                                                        SAMPLE_APPLICATION_STREAM_NAME,
                                                                                        credentialsProvider,
                                                                                        workerId);
            log("Calling kclConfig.withInitialPositionInStream()");
            kclConfig.withInitialPositionInStream(SAMPLE_APPLICATION_INITIAL_POSITION_IN_STREAM);

            IRecordProcessorFactory recordProcessorFactory = new KinesisConsumerFactory();
            Worker worker = new Worker(recordProcessorFactory, kclConfig);

            worker.run();

            System.out.printf("Running %s to process stream %s as worker %s...\n",
                    SAMPLE_APPLICATION_NAME,
                    SAMPLE_APPLICATION_STREAM_NAME,
                    workerId);

            int exitCode = 0;

        } catch (Exception t) {
            System.err.println("Caught throwable while processing data.");
            t.printStackTrace();
        }
    }
    
    private static void init() {
        // Ensure the JVM will refresh the cached IP values of AWS resources (e.g. service endpoints).
        java.security.Security.setProperty("networkaddress.cache.ttl", "60");

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        credentialsProvider = new ProfileCredentialsProvider();
        try {
            credentialsProvider.getCredentials();
            log("After loading AWS credentials");
        } catch (Exception e) {
            throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
                    + "Please make sure that your credentials file is at the correct "
                    + "location (~/.aws/credentials), and is in valid format.", e);
        }
    }

}
